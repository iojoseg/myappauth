package mx.tecnm.misantla.myappauth

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.google.firebase.auth.FirebaseAuth
import mx.tecnm.misantla.myappauth.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val MY_REQUEST_CODE: Int = 1234
    lateinit var  providers : List<AuthUI.IdpConfig>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        providers = Arrays.asList<AuthUI.IdpConfig>(
                AuthUI.IdpConfig.GoogleBuilder().build(),
                AuthUI.IdpConfig.FacebookBuilder().build()


        )

        TiposLogin()
        binding.btnCerrar.setOnClickListener {
            AuthUI.getInstance().signOut(this)
                    .addOnCompleteListener {
                        binding.btnCerrar.isEnabled = false
                        TiposLogin()
                    }
                    .addOnFailureListener {
                        e -> Toast.makeText(this,e.message,Toast.LENGTH_SHORT).show()
                    }
        }
    }

    private fun TiposLogin() {
        startActivityForResult(AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .build(),MY_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == MY_REQUEST_CODE){
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK){
                var user = FirebaseAuth.getInstance().currentUser
                Toast.makeText(this,""+user!!.email, Toast.LENGTH_SHORT).show()
                binding.btnCerrar.isEnabled = true
            }else{
                Toast.makeText(this,""+response!!.error!!.message, Toast.LENGTH_SHORT).show()
            }
        }
    }
}